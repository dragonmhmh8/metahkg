services:
  metahkg-nginx:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-nginx
    image: metahkg${COMPOSE_PROJECT_NAME}/nginx
    build:
      context: ./nginx
      dockerfile: ./Dockerfile
    tty: true
    ports:
      - ${port}:80
    restart: always
    environment:
      METAHKG_LINKS_NAME: metahkg${COMPOSE_PROJECT_NAME}-links
      METAHKG_SERVER_NAME: metahkg${COMPOSE_PROJECT_NAME}-server
      METAHKG_WEB_NAME: metahkg${COMPOSE_PROJECT_NAME}-web
      METAHKG_IMAGES_NAME: metahkg${COMPOSE_PROJECT_NAME}-images
      METAHKG_RLP_PROXY_NAME: metahkg${COMPOSE_PROJECT_NAME}-rlp-proxy
      domain: ${domain}
      LINKS_DOMAIN: ${LINKS_DOMAIN}
      IMAGES_DOMAIN: ${IMAGES_DOMAIN}
      RLP_PROXY_DOMAIN: ${RLP_PROXY_DOMAIN}
    depends_on:
      - metahkg-server
      - metahkg-web
      - metahkg-links
      - metahkg-images
      - metahkg-rlp-proxy
    networks:
      - metahkg-network
  metahkg-web:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-web
    image: metahkg${COMPOSE_PROJECT_NAME}/web
    build:
      context: ../metahkg-web
      dockerfile: ./Dockerfile
      args:
        REACT_APP_recaptchasitekey: ${REACT_APP_recaptchasitekey}
        REACT_APP_IMAGES_DOMAIN: ${IMAGES_DOMAIN}
        REACT_APP_RLP_PROXY_DOMAIN: ${RLP_PROXY_DOMAIN}
        REACT_APP_build: ${REACT_APP_build}
        REACT_APP_version: ${REACT_APP_version}
        REACT_APP_VAPID_PUBLIC_KEY: ${VAPID_PUBLIC_KEY}
        env: ${env}
    tty: true
    restart: always
    environment:
      port: 3002
      env: ${env}
      GCM_SENDER_ID: ${GCM_SENDER_ID}
    volumes:
      - ../metahkg-web/src:/home/user/src
      - ../metahkg-web/public:/home/user/public
    depends_on:
      - metahkg-server
      - metahkg-links
      - metahkg-images
      - metahkg-rlp-proxy
    networks:
      - metahkg-network
  metahkg-server:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-server
    image: metahkg${COMPOSE_PROJECT_NAME}/server
    build:
      context: ../metahkg-server
      dockerfile: ./Dockerfile
      args:
        env: ${env}
    tty: true
    restart: always
    environment:
      DB_URI: mongodb://${MONGO_USER}:${MONGO_PASSWORD}@metahkg${COMPOSE_PROJECT_NAME}-mongo:${MONGO_PORT}
      LINKS_DOMAIN: ${LINKS_DOMAIN}
      mailgun_key: ${mailgun_key}
      mailgun_domain: ${mailgun_domain}
      domain: ${domain}
      port: 3001
      recaptchasecret: ${recaptchasecret}
      jwtKey: ${jwtKey}
      cors: ${cors}
      env: ${env}
      register: ${register}
      VAPID_PUBLIC_KEY: ${VAPID_PUBLIC_KEY}
      VAPID_PRIVATE_KEY: ${VAPID_PRIVATE_KEY}
      GCM_API_KEY: ${GCM_API_KEY}
    volumes:
      - ./images:/home/user/images
      - ../metahkg-server/src:/home/user/src
    depends_on:
      - metahkg-mongo
    networks:
      - metahkg-network
  metahkg-links:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-links
    image: metahkg${COMPOSE_PROJECT_NAME}/links
    build:
      context: ../metahkg-links
      dockerfile: ./Dockerfile
    tty: true
    restart: always
    environment:
      port: 3003
      DB_URI: mongodb://${MONGO_USER}:${MONGO_PASSWORD}@metahkg${COMPOSE_PROJECT_NAME}-mongo:${MONGO_PORT}
      metahkgDomain: ${domain}
    networks:
      - metahkg-network
    depends_on:
      - metahkg-mongo
  metahkg-images:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-images
    image:  ghcr.io/willnorris/imageproxy:latest
    tty: true
    restart: always
    entrypoint: "/app/imageproxy -addr 0.0.0.0:3004 -cache /tmp/imageproxy"
    networks:
      - metahkg-network
    volumes:
      - ./imageproxy:/tmp/imageproxy
  metahkg-rlp-proxy:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-rlp-proxy
    image: metahkg${COMPOSE_PROJECT_NAME}/rlp-proxy
    build:
      context: ../rlp-proxy-rewrite
      dockerfile: ./Dockerfile
    tty: true
    restart: always
    environment:
      DB_URI: mongodb://${MONGO_USER}:${MONGO_PASSWORD}@metahkg${COMPOSE_PROJECT_NAME}-mongo:${MONGO_PORT}
      PORT: 3005
    depends_on:
      - metahkg-mongo
    networks:
      - metahkg-network
  metahkg-mongo:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-mongo
    image: mongo
    tty: true
    command: mongod --auth --port=${MONGO_PORT} --bind_ip_all
    restart: always
    ports:
      - ${MONGO_PORT}:${MONGO_PORT}
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${MONGO_USER}
      MONGO_INITDB_ROOT_PASSWORD: ${MONGO_PASSWORD}
      MONGO_INITDB_DATABASE: metahkg
    networks:
      - metahkg-network
    volumes:
      - ./data:/data/db
      - ./mongodb/mongo-init.sh:/docker-entrypoint-initdb.d/mongo-init.sh:ro
  metahkg-mongo-express:
    container_name: metahkg${COMPOSE_PROJECT_NAME}-mongo-express
    image: mongo-express
    tty: true
    restart: always
    ports:
      - ${MONGO_EXPRESS_PORT}:8081
    environment:
      ME_CONFIG_BASICAUTH_USERNAME: ${MONGO_USER}
      ME_CONFIG_BASICAUTH_PASSWORD: ${MONGO_PASSWORD}
      ME_CONFIG_MONGODB_ENABLE_ADMIN: false
      ME_CONFIG_MONGODB_SERVER: metahkg${COMPOSE_PROJECT_NAME}-mongo
      ME_CONFIG_MONGODB_PORT: ${MONGO_PORT}
      ME_CONFIG_MONGODB_AUTH_DATABASE: metahkg
      ME_CONFIG_MONGODB_AUTH_USERNAME: ${MONGO_USER}
      ME_CONFIG_MONGODB_AUTH_PASSWORD: ${MONGO_PASSWORD}
    networks:
      - metahkg-network
networks:
  metahkg-network:
    driver: bridge
